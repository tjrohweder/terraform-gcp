terraform {
  required_version = ">= 0.12.8"

  backend "gcs" {
    bucket      = "prime-analytics-terraform"
    prefix      = "production"
    credentials = "./creds/service_account.json"
  }
}

provider "google" {
  version     = ">= 2.14"
  project     = "prime-analytics-production"
  region      = var.gcp_region
  credentials = "./creds/service_account.json"
}

module "vpc" {
  source          = "./modules/vpc"
  k8s_subnet      = var.k8s_subnet
  general_subnet  = var.general_subnet
  k8s_nat_ips     = module.vpc.k8s_nat_ips
  general_nat_ips = module.vpc.general_nat_ips
}

module "gke" {
  source                     = "./modules/gke"
  cluster_name               = var.cluster_name
  gcp_region                 = var.gcp_region
  cluster_version            = var.cluster_version
  node_pool                  = var.node_pool
  gke_machine_type           = var.gke_machine_type
  k8s_network                = module.vpc.k8s_network
  k8s_subnetwork             = module.vpc.k8s_subnetwork
  disk_size_gb               = var.disk_size_gb
  disk_type                  = var.disk_type
  master_authorized_networks = var.master_authorized_networks
  master_network             = var.master_network
}

