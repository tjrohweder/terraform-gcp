resource "google_container_cluster" "gke-cluster" {
  name                     = var.cluster_name
  location                 = var.gcp_region
  remove_default_node_pool = true
  initial_node_count       = 1
  min_master_version       = var.cluster_version
  network                  = var.k8s_network
  subnetwork               = var.k8s_subnetwork

  ip_allocation_policy {
    use_ip_aliases = true
  }

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = var.master_network
  }

  master_authorized_networks_config {
    cidr_blocks {
      cidr_block   = var.master_authorized_networks
      display_name = "Public"
    }
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "03:00"
    }
  }

  addons_config {
    kubernetes_dashboard {
      disabled = true
    }
  }
}

resource "google_container_node_pool" "node-cluster-pool" {
  name       = var.node_pool
  location   = var.gcp_region
  cluster    = google_container_cluster.gke-cluster.name
  node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 15
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    machine_type = var.gke_machine_type
    disk_size_gb = var.disk_size_gb
    disk_type    = var.disk_type
    preemptible  = true

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/service.management",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

