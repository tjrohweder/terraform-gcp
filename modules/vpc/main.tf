locals {
  gcp_region = "us-central1"
}

resource "google_compute_network" "k8s" {
  name                    = "k8s"
  auto_create_subnetworks = false
  routing_mode            = "REGIONAL"
}

resource "google_compute_network" "general" {
  name                    = "general"
  auto_create_subnetworks = false
  routing_mode            = "GLOBAL"
}

resource "google_compute_subnetwork" "k8s_subnetwork" {
  name                     = "k8s-subnet-1"
  ip_cidr_range            = var.k8s_subnet
  region                   = local.gcp_region
  network                  = google_compute_network.k8s.self_link
  private_ip_google_access = true
  enable_flow_logs         = true
}

resource "google_compute_subnetwork" "general_subnetwork" {
  name                     = "general-subnet-1"
  ip_cidr_range            = var.general_subnet
  region                   = local.gcp_region
  network                  = google_compute_network.general.self_link
  private_ip_google_access = true
  enable_flow_logs         = true
}

resource "google_compute_firewall" "k8s_rules" {
  name    = "k8s-firewall"
  network = google_compute_network.k8s.self_link

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  allow {
    protocol = "tcp"
    ports    = ["19092"]
  }

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_tags = ["k8s"]
}

resource "google_compute_firewall" "general_rules" {
  name    = "general-firewall"
  network = google_compute_network.general.self_link

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_tags = ["general"]
}

resource "google_compute_address" "k8s-nat-ips" {
  count  = 2
  name   = "k8s-nat-ip-${count.index + 1}"
  region = google_compute_subnetwork.k8s_subnetwork.region
}

resource "google_compute_address" "general-nat-ips" {
  count  = 2
  name   = "general-nat-ip-${count.index + 1}"
  region = google_compute_subnetwork.general_subnetwork.region
}

resource "google_compute_router" "k8s-router" {
  name    = "k8s-router-1"
  region  = google_compute_subnetwork.k8s_subnetwork.region
  network = google_compute_network.k8s.self_link

  bgp {
    asn = "64514"
  }
}

resource "google_compute_router" "general-router" {
  name    = "general-router-1"
  region  = google_compute_subnetwork.general_subnetwork.region
  network = google_compute_network.general.self_link

  bgp {
    asn = "64514"
  }
}

resource "google_compute_router_nat" "k8s-nat" {
  name                               = "k8s-nat-1"
  router                             = google_compute_router.k8s-router.name
  region                             = google_compute_subnetwork.k8s_subnetwork.region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_PRIMARY_IP_RANGES"
  nat_ips                            = var.k8s_nat_ips
}

resource "google_compute_router_nat" "general-nat" {
  name                               = "general-nat-1"
  router                             = google_compute_router.general-router.name
  region                             = google_compute_subnetwork.general_subnetwork.region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_PRIMARY_IP_RANGES"
  nat_ips                            = var.general_nat_ips
}

resource "google_compute_network_peering" "peering1" {
  name         = "k8s-to-general"
  network      = google_compute_network.k8s.self_link
  peer_network = google_compute_network.general.self_link
}

resource "google_compute_network_peering" "peering2" {
  name         = "general-to-k8s"
  network      = google_compute_network.general.self_link
  peer_network = google_compute_network.k8s.self_link
}
