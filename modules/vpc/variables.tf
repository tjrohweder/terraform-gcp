variable "k8s_subnet" {
}

variable "general_subnet" {
}

variable "k8s_nat_ips" {
  type = list(string)
}

variable "general_nat_ips" {
  type = list(string)
}

