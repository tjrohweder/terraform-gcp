output "k8s_network" {
  value = google_compute_network.k8s.self_link
}

output "k8s_subnetwork" {
  value = google_compute_subnetwork.k8s_subnetwork.self_link
}

output "k8s_nat_ips" {
  value = google_compute_address.k8s-nat-ips.*.self_link
}

output "general_nat_ips" {
  value = google_compute_address.general-nat-ips.*.self_link
}

