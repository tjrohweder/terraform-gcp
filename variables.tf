variable "cluster_name" {
}

variable "gcp_region" {
}

variable "cluster_version" {
}

variable "node_pool" {
}

variable "gke_machine_type" {
}

variable "k8s_subnet" {
}

variable "general_subnet" {
}

variable "disk_size_gb" {
}

variable "disk_type" {
}

variable "master_authorized_networks" {
}

variable "master_network" {
}

