cluster_name = "production"

gcp_region = "us-central1"

cluster_version = "latest"

node_pool = "main-pool"

gke_machine_type = "n1-standard-2"

k8s_subnet = "10.0.0.0/20"

general_subnet = "10.1.0.0/20"

disk_size_gb = "20"

disk_type = "pd-ssd"

master_authorized_networks = "0.0.0.0/0"

master_network = "172.16.0.0/28"
